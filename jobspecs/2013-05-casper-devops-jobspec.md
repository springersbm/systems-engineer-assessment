DevOps Engineer/ Platform Engineer (SPR/SL/DOE)
===============================================

Springer, the world’s second largest publisher of scientific books and 
journals has built up an internal center of excellence in software 
development. This position, based in our London office, will be working 
closely with the entire development team, as well as other departments 
in Germany and the Netherlands

This team uses cutting-edge programming languages, databases, and a 
truly agile development process to create weekly releases one of the top 2000 
content websites in world, serving over 1 million page views a day to a 
global audience. The successful candidate for this role will be an 
enthusiastic, talented DevOps Engineer who will participate fully 
in the agile process.

As a DevOps engineer, you will play an essential role in providing the 
tools and expertise to enable both Development and Operational staff to 
build, support and deliver great online products that are fast, scalable, 
and highly available.

Main Duties
-----------

 * You will be working closely with both development and operations teams, 
   improving the systems and processes that define the software development 
   lifecyle and operational support processes.
 * You will be pairing with Developers and other DevOps Engineers to share 
   knowledge and best practice, and to work collaboratively for the continuous 
   improvement of both the platform and the processes around it.
 * You will be writing scripts and configuring the software that forms the 
   core of the Online Platform.
 * You will be putting to use all of your knowledge and experience of build 
   tools, continuous integration technologies, automated testing (functional and 
   load), monitoring systems, configuration management systems, etc.
 * Providing a second/third line of support as needed, but also taking an active 
   role in seeking out the root case of operational issues, and ensuring that 
   the right actions are taken to ensure they do not reoccur. You will want to 
   ensure that our systems are so robust that our support needs are proactive 
   rather than reactive.
 * As part of an Agile team, you will take part in daily stand-ups, showcases 
   of completed work, and regular retrospectives. Expressing your views and 
   opinions on all aspects of what we do will be essential, as these views 
   will be listened to at all levels so that we can adapt and be more successful 
   as a team. 

Person Specification
--------------------

Essential:

 * A thorough understanding of the systems that drive the Internet
 * A demonstrable passion for IT and Technology.	
 * Proven ability to design and support robust build, deployment and configuration 
   management systems for multi-tier web applications
 * An eagerness to learn new tools and technologies.
 * Strong knowledge of Linux
 * Must have an opinion on almost everything, coupled with the ability to express 
   it in a polite, collaborative and constructive manner.

Desirable:

 * Chef/Puppet configuration management
 * Ruby, Python, Perl or equivalent scripting language
 * Software Engineering background and familiarity with OO or Functional languages 
   and/or Systems Administration or Operations team experience
 * Understanding of software configuration issues
 * Scala knowledge
 * Experience with SQL or NoSQL databases
 * Experience with SCM tools such as Git, SVN, etc
 * Experience with unit testing and automated testing tools
 * Virtualization systems
 * Cloud computing
 * CDN Management/Configuration
 * Networking concepts, technologies, and protocols
 * Good knowledge of XML related technologies.
 * Working with large data sets.
 * Understanding of Agile methodologies, and having worked within an Agile team.

Assesment
---------

You might be required to do the assessment described 
[in this Trello board](https://trello.com/b/5qMF0d5A/). 

How to apply
------------

Please send your CV and covering letter, stating salary expectations and quoting 
the job reference (SPR/SL/DOE) to: 

   HR, Springer Science and Business Media Ltd, Floor 6,
   236 Gray’s Inn Road, London, 
   WC1X 8HB, or by email: ukjobs@springer.com 

Springer London is an Equal Opportunities Employer. We welcome all applications 
irrespective of race, gender, age, religion or belief, relationship status, 
pregnancy/maternity, sexual orientation or disability

