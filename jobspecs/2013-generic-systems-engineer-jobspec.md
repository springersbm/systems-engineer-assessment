Hello. We’re SpringerIT.
========================

Springer is a world-class international publisher. With over eight million content items available online to academic and professional institutions globally, our digital-first strategy embraces the modern economy.

We are a multi-disciplinary team working in a fast-paced and collaborative environment, who value honest opinion and open debate. We’re responsible for designing, building, testing and releasing our new content from our main site http://www.springer.com to our distribution site, SpringerLink http://link.springer.com, integrating all Springer’s key online offerings into one platform. We’re in the business of building enterprise-class web applications, and delivering a first-rate user experience is essential.

If you haven’t yet, meet one of our teams, London UK: http://joinit.springer.com

You...
------

* have knowledge in, large datasets using databases including SQL and NoSQL flavours such as CouchDB, Cassandra, and MarkLogic.
* enjoy working in Linux and open source environments.
* are familiar with virtualization systems like VMWare, EC2, and cloud platforms.
* have a thorough understanding of the technologies that drive the Internet.
* ideally have a software development and operations background.
* are able to design and support robust build, for multi-tier web applications.
* have experience with infrastructure as code technologies, for example, Chef or Puppet.
* are able to support a software delivery pipeline from continuous integration to automated deployment.

What you'll do...
-----------------

* contribute to improving the development and operational systems and processes.
* pair with developers and peers to share knowledge, best practices, and collaboratively improve our platform.
* configure the software which forms the foundation of our online platform, and use automation to ensure our processes are repeatable and scalable.
* keep up to date with emerging technologies to ensure the right tools are used for the job.
* take an active role in supporting and to finding the root cause of operational issues, and prevent from reoccurring.
* promote our DevOps practices within the company.
* strive to create an environment where developers and operations staff can work in mutually-supportive collaboration. unicorn.

Working here
------------

We build our software incrementally, iteratively, and continuously, so you’ll be able to see new work go live to over four million visitors around the globe every week. XP and Kanban heavily influence our flavour of agile and if something isn’t working, we change it.

Our teams are cross-discipline so that our work delivers value to both the business and our customers: business analysts and UX designers work with stakeholders to coordinate research and understand requirements, frontend developers and software engineers pair with quality analysts to develop the most robust solutions, and our platform engineers work across the organisation to ensure our infrastructure never lets us down.

Assesment
---------

You might be required to do the assessment described in this repo: https://bitbucket.org/springersbm/systems-engineer-assessment 

Roles
-----

We got roles based in:

* London UK, in our in Clerkenwell.
* Berlin, Germany
* Pune, India

How to apply
------------

Contact our HR departments

London:

    HR, Springer Science and Business Media Ltd, Floor 6,
    236 Gray’s Inn Road, London, 
    WC1X 8HB, or by email: ukjobs@springer.com 

Berlin:

    Springer Science+Business Media Deutschland GmbH
    Personalabteilung
    Frau Miriam an Haack <Miriam.anHaack@springer.com> 
    Heidelberger Platz 3
    14197 Berlin


