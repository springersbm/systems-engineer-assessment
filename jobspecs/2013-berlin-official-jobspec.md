Job Beschreibung
================

Original https://springer-career-com.becruiter.net/jobagent/search/job_details.aspx?jobid=64350

Systemadministrator / DevOps (m/w) 
----------------------------------

Professionals, Vollzeit, -, IT-Beratung & Entwicklung, Internet / Online / New Media, Softwareentwicklung
Springer Science+Business Media Deutschland GmbH, Berlin, Berlin

Wir über uns
------------

Springer Science+Business Media gehört mit mehr als 7.000 Mitarbeitern in etwa 25 Ländern weltweit zu den international renommiertesten Verlagsgruppen für Wissenschafts- und Fachliteratur.

Ihre Aufgaben
-------------

In einem agilen und stark wachsenden Umfeld sind Sie verantwortlich für den Betrieb unserer Java-Plattformen auf Basis von CentOS (Linux). Die IT-Landschaft beinhaltet neben unserer Online-Web-Plattformen auch verschiedene Backend Systeme, wie z.B. Coremedia CMS.
Als Systemadministrator/DevOp (m/w) sind Sie für die reibungslose Administration unserer IT-Infrastrukturen, Prozesse und Tools verantwortlich sowie für deren kontinuierliche Weiterentwicklung, um für die kommenden Herausforderungen optimal aufgestellt zu sein. In Ihrer Schnittstellenfunktion als DevOp gewährleisten Sie neben dem reibungslosen Betriebsablauf (ab Softwareentwicklungsprozess) eine verbindliche und klare Kommunikation zwischen unseren internationalen Infrastrukturteams und den Softwareentwicklern der Java-Plattformen in sämtlicher Betriebsfragestellungen. Ihr Aufgabengebiet umfasst im Einzelnen:

* Ausarbeitung und Betrieb von Architektur- / Infrastruktur-Lösungen in einem komplexen und dynamischen Umfeld
* Konfiguration und Administration von heterogenen Serversystemen, auf Basis von Linux (RHEL, CentOS)
* Bereitstellung standardisierter Betriebsumgebungen (Test, Stage, Live) incl. geeigneter Konfigurations- und Deployment-Tools sowie von Prozessen im Sinne von Continuous Delivery / Provisioning
* Aufbau und Betreuung von HA/FO Umgebungen für den Webauftritt
* Monitoring der Systeme auf Basis von Icinga(Nagios) und damit einhergehend Performance-Analysen und -Optimierungen sowie Behebung technischer Probleme aller Art

Die Erstellung von technischen Betriebskonzepten im Rahmen von Projekten runden Ihr Aufgabengebiet ab.

Unsere Anforderungen
--------------------

* Informatikstudium oder vergleichbare Fachrichtung
* Programmieren in Bash, Python, Ruby gehört zum täglichen Geschäft und ist erforderlich
* sehr gute Kenntnisse im Linux Umfeld. CentOS / RedHat Kenntnisse sind vorteilhaft
* Erfahrung im Betrieb und Aufbau von MySQL (Percona), Postgres und MongoDB Servern
* Knowhow im Bereich gängiger Konfigurationsmanagement Tools wie Chef oder Puppet
* Praktische Erfahrung in der Betreuung von Java-Applikationsservern sowie Apache, JBoss, Tomcat, Nginx, Varnish etc.
* Praktische Kenntnisse in Loadbalancer (F5 / HaProxy), Firewall (Cisco / iptables) und VPN’s (Cisco/openvpn)
* Grundlegende Erfahrung mit Icinga/Nagios für Monitioring und Metriken mit Graphite / Collectd sind von Vorteil
* Praktische Kenntnisse gängiger Virtualisierungslösungen (VMware, KVM, Xen, AWS)
* Erfahrung mit Atlassian Tools – Jira, Confluence, Crowd – sind von Vorteil

Zudem verfügen Sie über sehr gute Deutsch- und Englischkentnisse und zeichnen sich neben ausgeprägten analytischen-konzeptionellen Fähigkeiten durch eine eigenständige, effiziente und umsetzungssorientierte Arbeitsweise aus. Persönlich runden ausgesprochene Teamplayer- und Kommunikationsqualitäten sowie ein hohes Maß an Proaktivität und Belastbarkeit Ihr Profil ab. 

Wir freuen uns auf Ihre Online-Bewerbung!
-----------------------------------------

Sollte Ihnen das nicht möglich sein, können Sie uns Ihre vollständigen Bewerbungsunterlagen auch per Post zukommen lassen:

    Springer Science+Business Media Deutschland GmbH
    Personalabteilung
    Frau Miriam an Haack <Miriam.anHaack@springer.com> 
    Heidelberger Platz 3
    14197 Berlin


