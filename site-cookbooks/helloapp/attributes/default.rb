
# Location of the war file to install
default[:helloapp][:war_url] = "https://s3-eu-west-1.amazonaws.com/springer-public-space/springer-hello-world_2.10-0.1.0-SNAPSHOT.war"
# Checksum of the war file (md5sum)
default[:helloapp][:war_chksum] = "85af1f323469ada9494358a52d65e1ab"

# Application settings
default[:helloapp][:db_server] = "mydb.enterprise.com"
default[:helloapp][:db_port] = "3306"
default[:helloapp][:db_login] = "hellouser"
default[:helloapp][:db_pass] = "hellopass"
default[:helloapp][:date_provide_url] = "http://sleepy-thicket-8107.herokuapp.com"
default[:helloapp][:threadpool_size] = 200
default[:helloapp][:admin_login] = "admin"
