maintainer       "Springer"
maintainer_email "casper.root@springer.com"
license          "All rights reserved"
description      "Installs/Configures Hello World App from Springer"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"

depends          "application_java"
