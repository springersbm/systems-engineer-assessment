#
# Cookbook Name:: helloapp
# Recipe:: default
#
# Copyright 2013, Springer
#
# All rights reserved - Do Not Redistribute
#
require_recipe "tomcat"

application "helloapp" do
  path "/usr/local/helloapp"
  repository node[:helloapp][:war_url]
  revision node[:helloapp][:war_chksum]
  owner node["tomcat"]["user"]
  group node["tomcat"]["group"]  
  scm_provider Chef::Provider::RemoteFile::Deploy
  java_webapp do
    context_template "helloapp-context.xml.erb"
  end
  tomcat
  action :deploy
end