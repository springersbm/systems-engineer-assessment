name "base"
description "Role including the base OS configuration"

run_list [ 
  "recipe[ntp]",
  "recipe[debian]" 
]

default_attributes ({
  'debian' => {
     'mirror'=> "http://ftp.uk.debian.org/debian/"
   }
})
override_attributes {
}
