name "helloapp"
description "Server that includes the Springer Hello World App"

run_list [
  "role[base]",
  "recipe[helloapp]"
]

default_attributes ({
  "tomcat" => { 
    "java_options" => "-Xmx96m -Djava.awt.headless=true",
    "base_version" => "7" 
  }
})  
override_attributes {
}
