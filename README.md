Platform Engineering Exercise
==================================

This repository is part of the Springer Nature candidate assessment for Systems Engineers and Platform Developers.

Job description
---------------
At Springer Nature, the world’s second largest publisher of scientific books
and journals we always look for great engineers to join our Platform Engineering
team or one of the development team.

At Springer Nature you will work in a international development team using a true agile development
process and implementing the best DevOps recommendations and tooling. Here you will play an essential
role in providing the tools and expertise to enable both Development and Operational staff
to build, support and continuously deliver great online products that are fast, scalable, and
highly available.

The IT department operates in a fairly flat organisation, and you will have input and impact
at all scales. We look for somebody who is not scared to change, and will promote and defend
any idea they believe in but equally open to dialog and agreement.

You can find the current open positions in the [Springer main site](http://www.springer.com/about+springer/career/all+vacancies?SGWID=0-1717713-0-0-0)
and the job portals. You can also find current and old positions [here](https://bitbucket.org/springersbm/systems-engineer-assessment/src/master/jobspecs).

Of course, even if there are not open positions, we are always more than happy
to talk with any good candidate. In that case, this assessment will be a good
presentation.


Trello board
------------

The instructions to use this repository are in the first card of
[this trello board](https://trello.com/b/5qMF0d5A/)


Technical details
=================

Setting the environment
-----------------------

Note: This assessment has been tested on Ubuntu 13.04. It should work on other platforms.

Base dependencies:

 * Ruby (Tested on 1.9.3p547, but should work on any ruby 1.9). We suggest installing RVM. https://rvm.io
```
curl -L https://get.rvm.io | bash -s stable --ruby=1.9.3
rvm use 1.9.3@springer-assessment --create
```
 * [gem bundler (Tested on 1.6.5)](http://gembundler.com/). Run `gem install bundler` to install

 * [VirtualBox (Tested on 4.3.10)](https://www.virtualbox.org)

 * [Vagrant (Tested on 1.6.3)](http://www.vagrantup.com/)

Clone the project using git and download ruby dependencies:
```
bundle install
```

We use librarian to install external cookbooks: https://github.com/applicationsonline/librarian-chef

Download external cookbooks:
```
bundle exec librarian-chef install
```

Starting Vagrant box
--------------------

Go to `tools/vagrant/helloapp` and run `vagrant up`

```
cd tools/vagrant/helloapp
vagrant up
```

While the vagrant is running you can rerun chef with:
```
cd tools/vagrant/helloapp
vagrant provision
```

Ports and network used
-----------
This vagrant box maps the following ports:
 - 8080 to 8080
 - 8000 to 8000
 - 80 to 80
 - 2222 to 22 (ssh)

You must have these ports free in your PC. Optionally, you can change the port numbers for the host in `tools/vagrant/helloapp/Vagrantfile`

You need full internet access to connect. Also, notice that it will download lots of data.

Testing
------

Connect to [helloapp in the port 8080](http://localhost:8080/helloapp). You should see a helloapp page. **This application might fail with a timeout error**, that is expected and is part of the task. If you refresh the page, it should eventually work.

You can also login executing `vagrant ssh`

About the repository
====================

This repository uses chef-solo in order to setup a Vagrant Virtual Box definition with a Debian installation, running a Scala based web application on tomcat.

Technologies used:
 - ruby and bundler
 - Chef
 - chef-librarian
 - knife-solo
 - vagrant


Extras
======

Creating vagrant Box
--------------------

We will create a Debian 7.0 i386 using [veewee](https://github.com/jedi4ever/veewee),
that you can find in `tools/veewee`

```
cd tools/veewee
bundle install
```

We used the template 'Debian-7.0-b4-i386-netboot', but adapted it for the final stable release.

```
bundle exec veewee vbox define 'Debian-7.0.i386' 'Debian-7.0-b4-i386-netboot'
```

To create the Box based on the definition under definitions/Debian-7.0.i386:

```
veewee vbox build 'Debian-7.0.i386'
```

FIXME: You must manually select the keyboard in the first screen.

This step takes a long time. Once done, you must export the VM to a .box file:

```
# In theory this should work, but the export command is missing 	:-?
# veewee vbox build 'Debian-7.0.i386'
# We can use vagrant directly
mkdir boxes
vagrant package --base Debian-7.0.i386 --output 'boxes/Debian-7.0.i386.box'
```

The final image has been uploaded to [S3](https://s3-eu-west-1.amazonaws.com/springer-public-space/Debian-7.0.i386.box)